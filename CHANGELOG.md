## [6.8.10] - 2023-07-07
### Added
- Enrich error message with more data, and support all error objects from TheoPlayer

## [6.8.9] - 2023-01-12
### Fixed
- Fix issue with player object on getThroughput method

## [6.8.8] - 2023-01-11
### Fixed
- Removed request/responseInterceptor on unregisteredListeners method

## [6.8.7] - 2023-01-10
### Fixed
- Removed fixed mediaDuration on a sample page
### Added
- Removed request/responseInterceptor to detect throughput value

## [6.8.6] - 2022-12-23
### Fixed
- Only track buffering with waiting like shaka integration
- Only track stalls when joined like the shaka integration
- Track stalls with waiting event
### Added
- Add support to send triggeredEvents
- Use THEO bandwidth estimate if available
### Library
- Packaged with `lib 6.8.39`

## [6.8.5] - 2022-06-27
### Fixed
- Wrong behaviour with seek, buffer and pause events interacting between them
### Library
- Packaged with `lib 6.8.24`

## [6.8.4] - 2022-01-26
### Fixed
- Fake view reported after postrolls if wrong timeupdate event was triggered
### Library
- Packaged with `lib 6.8.11`

## [6.8.3] - 2022-01-20
### Fixed
- Postroll position detection

## [6.8.2] - 2021-12-21
### Removed
 - Offline resource error is now non fatal
### Added
 - Alternative way to detect current ad, adsadapter refactor
### Lib
 - Packaged with `lib 6.8.10`

## [6.8.1] - 2021-12-02
### Fixed
 - Refactor of the ads adapter, trying to avoid invalid access to the content adapter and to wrong values of ad duration
### Lib
 - Packaged with `lib 6.8.9`

## [6.8.0] - 2021-11-08
### Added
 - Support to new and improved error reporting
### Lib
 - Packaged with `lib 6.8.6`

## [6.7.11] - 2021-03-29
### Fixed
 - Trycatch getters to prevent crashes when player is destroyed
 - Destroy listener to unregister listeners when player is destroyed to prevent invalid calls
### Lib
 - Packaged with `lib 6.7.30`

## [6.7.10] - 2021-02-05
### Added
 - Additional case for audio language detection when there is no valid language
### Fixed
 - Warning on ads adapter when trying to get the ad resource
### Lib
 - Packaged with `lib 6.7.27`

## [6.7.9] - 2021-02-03
### Added
 - Audio language detection
### Lib
 - Packaged with `lib 6.7.26`

## [6.7.8] - 2021-01-28
### Fixed
 - Removed adsadapter when destroying adapter

## [6.7.7] - 2020-11-23
### Lib
 - Packaged with `lib 6.7.23`

## [6.7.6] - 2020-11-11
### Lib
 - Packaged with `lib 6.7.22`

## [6.7.5] - 2020-06-29
### Added
 - `parsedResource` detection without `parse.manifest` option
### Lib
 - Packaged with `lib 6.7.11`

## [6.7.4] - 2020-06-08
### Fixed
 - Prevented creation of fake views at the beggining of the content load
### Lib
 - Packaged with `lib 6.7.7`

## [6.7.3] - 2020-05-20
### Fixed
 - Check for ads object when setting the adsadapter to prevent errors

## [6.7.2] - 2020-05-13
### Fixed
 - Wrong throughput reported sometimes 
### Lib
 - Packaged with `lib 6.7.6`

## [6.7.1] - 2020-04-20
### Lib
 - Packaged with `lib 6.7.5`

## [6.7.0] - 2020-03-02
### Added
- Dropped frames
- Small refactor for adapter and adsadapter
### Lib
 - Packaged with `lib 6.7.0`

## [6.5.8] - 2020-01-23
### Fixed
- Fatal errors not being reported (stop not sent)
- Adplayhead fix for ad events
### Lib
 - Packaged with `lib 6.5.25`

## [6.5.7] - 2019-10-14
### Fixed
- Buffers being reported sometimes after an ad break.
- Error trying to get the duration of the ad just when it starts
- Break ended with error not being closed

## [6.5.6] - 2019-10-07
### Fixed
- Fake view created after content ended
### Lib
 - Packaged with `lib 6.5.16`

## [6.5.5] - 2019-09-10
### Added
 - OFFLINE_SOURCE is non fatal
### Fixed
 - Prevent seek to change pause duration
### Lib
 - Packaged with `lib 6.5.14`

## [6.5.4] - 2019-07-26
 ### Added
 - Support live latency if reported
### Lib
 - Packaged with `lib 6.5.8`

## [6.5.3] - 2019-07-17
 ### Added
 - Support for adTitle and adResource (IMA)
### Lib
 - Packaged with `lib 6.5.7`

## [6.5.2] - 2019-06-17
 ### Fixed
 - Wrong implementation for skippable ads
 - Removed expected ads getter
 - Jointime calculation with prerolls
### Lib
 - Packaged with `lib 6.5.4`

## [6.5.1] - 2019-06-06
 ### Fixed
 - Fake start sent during ads
 - Unnecesary code for resume and seek
 - Playhead issue with ads
 ### Lib
 - Packaged with `lib 6.5.3`

## [6.5.0] - 2019-05-31
 ### Lib
 - Packaged with `lib 6.5.2`

## [6.4.6] - 2019-04-17
 ### Fixed
 - Joining views after errors, for safari

## [6.4.5] - 2019-04-09
 ### Fixed
 - Creating views after errors, for safari

## [6.4.4] - 2019-04-08
### Lib
 - Packaged with `lib 6.4.23`
 ### Fixed
 - Checked null references

## [6.4.3] - 2019-02-15
### Lib
 - Packaged with `lib 6.4.16`
 ### Fixed
 - Seek never sent

## [6.4.2] - 2018-09-06
### Lib
 - Packaged with `lib 6.4.8`
 ### Fixed
 - respondWith in responseListener

## [6.4.1] - 2018-09-06
### Lib
 - Packaged with `lib 6.4.7`
 ### Added
 - Listener for sourcechange

## [6.4.0] - 2018-08-16
### Lib
 - Packaged with `lib 6.4.1`

## [6.2.3] - 2018-06-06
### Added
 - Support for adPause and adResume

## [6.2.2] - 2018-05-17
### Changed
- Reporting error messages in error code
### Lib
- Packaged with `lib 6.2.6`

## [6.2.1] - 2018-04-23
### Fixed
- Stop views after error
### Lib
- Packaged with `lib 6.2.2`

## [6.2.0] - 2018-04-09
### Added
- Included throughput calculation
- Rendition for mp4 content
### Lib
- Packaged with `lib 6.2.0`

## [6.1.2] - 2018-02-12
### Fix
- IMA ads support
### Lib
- Packaged with `lib 6.1.12`

## [6.1.1] - 2018-02-08
### Fix
- Fixed live detection for live without ads

## [6.1.0] - 2018-02-06
### Lib
- Packaged with `lib 6.1.11`
### Added
- Ad adapter
### Fix
- Fixed issues with error detection and live detection

## [6.0.0] - 2017-07-24
### Added
- First release
- Packaged with `lib 6.0.4`
