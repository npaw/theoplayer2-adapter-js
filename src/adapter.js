/* global THEOplayer, theoplayer */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.TheoPlayer2 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayrate: function () {
    try {
      return this.player.playbackRate
    } catch (err) {
      //nothing
    }
    return 0
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = null
    try {
      if (this.player.ads && this.player.ads.playing) {
        ret = this.lastPlayhead
      } else {
        this.lastPlayhead = this.player.currentTime
        ret = this.player.currentTime
      }
    } catch (err){
      //nothing
    }
    return ret
  },

  getIsLive: function () {
    return isNaN(this.player.duration) ? null : this.player.duration === Infinity
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (this.player.ads && this.player.ads.playing) {
      ret = this.lastDuration || ret
    } else {
      this.lastDuration = this.player.duration
      ret = this.lastDuration
    }
    return ret
  },

  getThroughput: function () {
    if (this.player) {
      if (this.player.metrics && this.player.metrics.currentBandwidthEstimate) {
        return this.player.metrics.currentBandwidthEstimate
      }
    }
    return null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = -1
    var qty = this.getActiveQuality()
    if (qty && qty.bandwidth) {
      ret = qty.bandwidth
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    this._setLanguage()
    var ret = null
    var qty = this.getActiveQuality()
    if (qty) {
      if (qty.name && isNaN(qty.name)) {
        ret = qty.name
      } else {
        ret = youbora.Util.buildRenditionString(qty.width, qty.height, qty.bandwidth)
      }
    } else if (this.player.videoHeight && this.player.videoWidth) {
      ret = youbora.Util.buildRenditionString(this.player.videoWidth, this.player.videoHeight, 0)
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    if (this.player.src && !this.previousSrc) {
      this.previousSrc = this.player.src
    }
    return this.player.src || null
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var version = 'THEOplayer'
    if (typeof THEOplayer !== 'undefined') {
      version += THEOplayer.version
    } else if (typeof theoplayer !== 'undefined') {
      version += theoplayer.version
    }
    return version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'THEOplayer'
  },

  getActiveQuality: function () {
    var vTracks = this.player.videoTracks
    for (var i = 0; i < vTracks.length; i++) {
      var currentTrack = vTracks.item(i)
      if (currentTrack.enabled) {
        return currentTrack.activeQuality
      }
    }
    return {}
  },

  getLatency: function () {
    var ret = null
    if (this.getIsLive() && this.player.currentProgramDateTime !== null) {
      var now = new Date().getTime()
      ret = now - this.player.currentProgramDateTime
    }
    return ret
  },

  getDroppedFrames: function () {
    return this.player.metrics ? this.player.metrics.droppedVideoFrames : null
  },

  getURLToParse: function () {
    return this.parsedUrl
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(false, true)

    // Register listeners
    this.references = {
      play: this.playListener.bind(this),
      playing: this.playingListener.bind(this),
      pause: this.pauseListener.bind(this),
      seeking: this.seekingListener.bind(this),
      ended: this.endedListener.bind(this),
      error: this.errorListener.bind(this),
      readystatechange: this.stateChangeListener.bind(this),
      sourcechange: this.endedListener.bind(this),
      timeupdate: this.timeUpdateListener.bind(this),
      ratechange: this.playrateListener.bind(this),
      waiting: this.waitingListener.bind(this)
    }

    this.networkReferences = {
      offline: this.offlineListener.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
    if (this.player.network) {
      for (var key2 in this.networkReferences) {
        this.player.network.addEventListener(key2, this.networkReferences[key2])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    if (this.plugin){
      this.plugin.removeAdsAdapter()
    }
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
    if (this.player && this.player.network && this.networkReferences) {
      for (var key2 in this.networkReferences) {
        this.player.network.removeEventListener(key2, this.networkReferences[key2])
      }
      delete this.networkReferences
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireInit({}, 'playListener')
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    if (!this.plugin.getAdsAdapter() || !this.plugin.getAdsAdapter().flags.isStarted) {
      if (this.flags.isSeeking) {
        this.fireSeekEnd({}, 'playingListener')
      }
      if (this.flags.isBuffering) {
        this.fireBufferEnd({}, 'playingListener')
      }
      this.fireResume({}, 'playingListener')
      if (!this.player.ads || !this.player.ads.playing) {
        this.ended = false
        this.fireStart(this._segmentUrl(), 'playingListener')
      }
      this.fireJoin({}, 'playingListener')
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isSeeking) {
      this.firePause({}, 'pauseListener')
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    if (!this.flags.isPaused) {
      this.fireSeekBegin({}, false, 'seekingListener')
    }
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    if (this.flags.isStarted) {
      this.plugin.fireStop({}, 'endedListener')
      this.ended = true
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var eventErrorObjectCode,playerErrorObjectCode,playerErrorCode
    ,eventErrorObjectMessage,playerErrorObjectMessage,playerErrorMessage
    ,eventErrorObjectCause,playerErrorObjectCause,playerErrorCause;

    if (e.errorObject) {
      eventErrorObjectCode = e.errorObject.code
      eventErrorObjectMessage = e.errorObject.message
      eventErrorObjectCause = !!e.errorObject.cause && e.errorObject.cause.message
    } else {
      eventErrorObjectMessage = e.error
    }

    if (this.player.errorObject) {
      playerErrorObjectCode = this.player.errorObject.code
      playerErrorObjectMessage = this.player.errorObject.message
      playerErrorObjectCause = !!this.player.errorObject.cause && this.player.errorObject.cause.message
    }

    if (this.player.error) {
      playerErrorCode = this.player.error.code
      playerErrorMessage = this.player.error.message
      playerErrorCause = this.player.error.cause
    }

    var messages = []
    if (eventErrorObjectMessage) messages.push('event: ' + eventErrorObjectMessage)
    if (playerErrorObjectMessage) messages.push('player.errorObject: ' + playerErrorObjectMessage)
    if (playerErrorMessage) messages.push('player.error: ' + playerErrorMessage)

    var causes = []
    if (eventErrorObjectCause) causes.push('event: ' + eventErrorObjectCause)
    if (playerErrorObjectCause) causes.push('player.errorObject: ' + playerErrorObjectCause)
    if (playerErrorCause) causes.push('player.error: ' + playerErrorCause)

    var codes = []
    if (eventErrorObjectCode) codes.push('event: ' + eventErrorObjectCode)
    if (playerErrorObjectCode) codes.push('player.errorObject: ' + playerErrorObjectCode)
    if (playerErrorCode) codes.push('player.error: ' + playerErrorCode)

    var code = eventErrorObjectCode || playerErrorObjectCode || playerErrorCode || 'GENERIC_ERROR'
    var msg = '[MESSAGES] ' + messages.join(' - ') + ' - [CAUSES] ' + causes.join(' - ')  + ' - [CODES] ' + codes.join(' - ')

    this.fireFatalError(code, msg, undefined, undefined, 'errorListener')
  },

  /** Listener for 'offline' event. */
  offlineListener: function (e) {
    this.fireError('OFFLINE_SOURCE', 'OFFLINE_SOURCE', undefined, undefined, 'offlineListener')
  },

  timeUpdateListener: function (e) {
    if (!this.player.ads || !this.player.ads.playing) {
      if (!this.flags.isStarted && e.currentTime && !this.ended) {
        if (this.getIsLive() || e.currentTime + 1 < this.getDuration()) {
          this.fireStart(this._segmentUrl(), 'timeUpdateListener')
          this.fireJoin(this._segmentUrl(), 'timeUpdateListener')
        }
      }
    }
  },

  playrateListener: function (e) {
    if (this.monitor) {
      this.monitor.skipNextTick()
    }
  },

  waitingListener: function (e) {
    if (!this.flags.isPaused && !this.flags.isSeeking) {
      this.fireBufferBegin({}, false, 'waitingListener')
    }
  },

  /** Listener for 'readystatechange' event. */
  stateChangeListener: function (e) {
    if (!this.plugin.getAdsAdapter() && this.player.ads) {
      this.plugin.setAdsAdapter(new youbora.adapters.TheoPlayer2.NativeAdsAdapter(this.player))
    }
    // Used when changing the source dinamically
    if (e.currentTime === 0 && e.readyState === 0 &&
      ((this.player.src !== this.previousSrc) && (this.player.src !== undefined))) {
      this.fireStop({}, 'stateChangeListener')
      this.previousSrc = this.player.src
    }
  },

  _segmentUrl: function () {
    var ret = null
    if (this.plugin && this.plugin.options && !this.plugin.options['parse.manifest'] && this.parsedUrl) {
      ret = { parsedResource: this.parsedUrl }
    }
    return ret
  },

  _setLanguage: function () {
    // Since there is still no getLanguage method for adapters, we workaround this getting the language
    // and updating the option every time getrendition is called (before pings)
    if (this.plugin) {
      var tracks = this.player.audioTracks
      for (track in tracks) {
        var t = tracks[track]
        if (t.activeQuality) {
          this.plugin.options['content.language'] = t.language === 'und' ? t.name || t.label : t.language;
        }
      }
    }
  }
},
// Static members
{
  NativeAdsAdapter: require('./ads/generic')
}
)

module.exports = youbora.adapters.TheoPlayer2
