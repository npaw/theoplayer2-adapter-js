var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var NativeAdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-THEOplayer2-ads'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.currentTime
  },

  getTitle: function () {
    var current = this._getCurrentAd()
    return current && current.title ? current.title : 'unknown'
  },

  /** Override to return video duration */
  getDuration: function () {
    var current = this._getCurrentAd()
    return current && current.duration ? current.duration : this.player.duration
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    var current = this._getCurrentAd()
    if (current) {
      if (current.mediaUrl) {
        ret = current.mediaUrl
      } else if (current.mediaFiles){
        ret = current.mediaFiles[0].resourceURI || current.mediaFiles[0].contentURL
      }
    }
    return ret
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    var pos = youbora.Constants.AdPosition
    var ret = pos.Midroll
    var plugin = this.plugin
    if (plugin) {
      var adapter = plugin.getAdapter()
      if (adapter) {
        if (!adapter.flags.isJoined) {
          ret = pos.Preroll
        } else if (plugin.getPlayhead() + (plugin._ping.interval / 1000) > (plugin.getDuration() || 0)) {
          ret = pos.Postroll
        }
      }
    }
    return ret
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.player.ads.scheduledAdBreaks) {
      ret = this.player.ads.scheduledAdBreaks.length || ret
    }
    return ret
  },

  getGivenAds: function () {
    return this.actualSlot ? this.actualSlot.ads.length : null
  },

  /* getBreaksTime: function() {
      var adbreaks = this.player.ads.scheduledAdBreaks
      var timelist = []
      for (adbreakindex in adbreaks) {
          var value = adbreaks[adbreakindex].timeOffset
          if (value === -1) {
              timelist.push(this.plugin.getAdapter().getDuration())
          } else {
              timelist.push(value)
          }
      }
      return timelist
      // Midroll time can be NaN and postrolls -1 when content duration is not available yet,
      //so this method would be not reliable
  }, */

  getIsVisible: function () {
    try {
      var tag = this.player ? this.player.element : null
    } catch (err) {
      // nothing
    }
    return tag ? youbora.Util.calculateAdViewability(tag) : false
  },

  getAudioEnabled: function () {
    return !this.player.muted && this.player.volume > 0
  },

  getIsSkippable: function () {
    var current = this._getCurrentAd()
    return current && current.skipOffset ? current.skipOffset >= 0 : false
  },

  getIsFullscreen: function () {
    var ret = false
    if (this.player.element && this.player.element.clientWidth && this.player.element.clientHeight) {
      ret = (window.innerHeight <= this.player.element.clientHeight + 30 && window.innerWidth <= this.player.element.clientWidth + 30)
    }
    return ret
  },

  _getCurrentAd: function () {
    var ads = this.player.ads
    var ret = null
    if (ads) {
      if (ads.currentAds && ads.currentAds[0]) {
        ret = ads.currentAds[0]
      }
      else if (ads && ads.currentAdBreak && ads.currentAdBreak.ads[this.adCount - 1]) {
        ret = ads.currentAdBreak.ads[this.adCount - 1]
      }
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    if (this.player.ads) {
      // Console all events if logLevel=DEBUG
      youbora.Util.logAllEvents(this.player.ads)

      // Playhead monitor for buffer
      this.monitorPlayhead(true, false, 2000)

      // Register listeners
      this.references = {
        adbegin: this.playAdListener.bind(this),
        adend: this.endedAdListener.bind(this),
        aderror: this.errorAdListener.bind(this),
        adskip: this.skipAdListener.bind(this),
        adbreakbegin: this.breakBeginListener.bind(this),
        adbreakend: this.breakEndListener.bind(this)
      }

      this.playerReferences = {
        pause: this.pauseAdListener.bind(this),
        playing: this.resumeAdListener.bind(this),
        play: this.resumeAdListener.bind(this),
        timeupdate: this.quartileListener.bind(this)
      }

      for (var key in this.references) {
        this.player.ads.addEventListener(key, this.references[key])
      }

      for (var key2 in this.playerReferences) {
        this.player.addEventListener(key2, this.playerReferences[key2])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.player.ads && this.references) {
      for (var key in this.references) {
        this.player.ads.removeEventListener(key, this.references[key])
      }
      delete this.references
    }

    if (this.player && this.playerReferences) {
      for (var key2 in this.playerReferences) {
        this.player.removeEventListener(key2, this.playerReferences[key2])
      }
      delete this.playerReferences
    }
  },

  /** Listener for 'adbegin' event. */
  playAdListener: function (e) {
    this.adCount = (this.adCount || 0) + 1
    var adapter = this.plugin.getAdapter()
    if (adapter) {
      adapter.ended = false
      adapter.fireInit()
      adapter.firePause()
    }
    this.fireStart({ adPlayhead: '0' })
    this.fireJoin({ adPlayhead: '0' })
    this.duration = this.getDuration()
  },

  /** Listener for 'aderror' event. */
  errorAdListener: function (e) {
    this.fireError()
    if (!this.flags.isStarted) {
      this.fireBreakStop()
    }
  },

  /** Listener for 'adend' event. */
  endedAdListener: function (e) {
    this.fireStop({
      adPlayhead: this.duration
    })
  },

  /** Listener for 'adskip' event. */
  skipAdListener: function (e) {
    this.fireSkip()
  },

  pauseAdListener: function (e) {
    this.firePause()
  },

  resumeAdListener: function (e) {
    this.fireResume()
  },

  breakBeginListener: function (e) {
    this.adCount = 0
    this.actualSlot = e.ad
  },

  breakEndListener: function (e) {
    this.fireBreakStop()
    var adapter = this.plugin.getAdapter()
    if (adapter) {
      adapter.fireResume()
    }
  },

  quartileListener: function (e) {
    var pos = this.getPlayhead() / this.duration
    if (pos >= 0.25) {
      this.fireQuartile(1)
      if (pos >= 0.5) {
        this.fireQuartile(2)
        if (pos >= 0.75) {
          this.fireQuartile(3)
        }
      }
    }
  }
})

module.exports = NativeAdsAdapter
